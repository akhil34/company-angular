import { Component, OnInit } from '@angular/core';
import {NotificationService} from '../services/notification/notification.service';
import {AuthService} from '../services/auth/auth.service';
import {LocalStorageService} from '../services/auth/local-storage.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username = '';
  password = '';
  constructor(private notification: NotificationService,
              private authService: AuthService,
              private localStorageService: LocalStorageService,
              private router: Router) { }

  ngOnInit(): void {
    console.log('On login component')
  }
  verifyForm() {
    if (this.username === '' || this.password === '') {
      this.notification.showNotification('Please enter email and password', 'danger')
      return
    }
    this.login()
  }
  login() {
    this.authService.login(this.username, this.password).subscribe((user: any) => {
        this.localStorageService.setToken(user.token)
        this.notification.showNotification('Login Successful', 'success')
        this.router.navigate(['/company/list'])
    }, (err) => {
      this.notification.showNotification('Invalid Password' , 'danger')
    })
  }
}
