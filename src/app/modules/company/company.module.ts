import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompanyListComponent } from './pages/company-list/company-list.component';
import {RouterModule} from '@angular/router';
import {CompanyRoutes} from './company.routing';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatRippleModule} from '@angular/material/core';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatTooltipModule} from '@angular/material/tooltip';
import { CompanyAddModalComponent } from './components/company-add-modal/company-add-modal.component';
import {MatDialogModule} from '@angular/material/dialog';
import { CompanySearchComponent } from './pages/company-search/company-search.component';
import {MatAutocompleteModule} from '@angular/material/autocomplete';


@NgModule({
  declarations: [
    CompanyListComponent,
    CompanyAddModalComponent,
    CompanySearchComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(CompanyRoutes),
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatRippleModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    MatDialogModule,
    MatAutocompleteModule
  ]
})
export class CompanyModule { }
