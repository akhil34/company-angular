import {Component, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {CompanyService} from '../../../../services/company/company.service';
import {MatDialog} from '@angular/material/dialog';
import {NotificationService} from '../../../../services/notification/notification.service';


@Component({
  selector: 'app-company-search',
  templateUrl: './company-search.component.html',
  styleUrls: ['./company-search.component.css']
})
export class CompanySearchComponent implements OnInit {
  companyLists: any = [];
  myControl = new FormControl();
  options: string[] = [];
  filteredOptions: Observable<string[]>;
  constructor(private companyService: CompanyService,
              private notification: NotificationService) { }

  ngOnInit(): void {
    this.fetchCompanies()
    this.filteredOptions = this.myControl.valueChanges
        .pipe(
            startWith(''),
            map(value => this._filter(value))
        );
  }
  fetchCompanies() {
    this.companyService.getAllCompanies().subscribe(async (res: any) => {
      this.companyLists = await res.companies
      this.companyLists.forEach(company => {
        this.options.push(company.title)
      })
    })
  }
  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }
}
