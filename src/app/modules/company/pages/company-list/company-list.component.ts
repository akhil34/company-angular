import { Component, OnInit } from '@angular/core';
import {CompanyService} from '../../../../services/company/company.service';
import {CompanyAddModalComponent} from '../../components/company-add-modal/company-add-modal.component';
import {MatDialog} from '@angular/material/dialog';
import {NotificationService} from '../../../../services/notification/notification.service';

@Component({
  selector: 'app-company-list',
  templateUrl: './company-list.component.html',
  styleUrls: ['./company-list.component.css']
})
export class CompanyListComponent implements OnInit {
  companyLists: any = [];
  constructor(private companyService: CompanyService,
              public dialog: MatDialog,
              private notification: NotificationService) { }

  ngOnInit(): void {
  this.fetchCompanies()
  }
  fetchCompanies() {
    this.companyService.getAllCompanies().subscribe((res: any) => {
      this.companyLists = res.companies
    })
  }
  companyAdd() {
    const dialogRef = this.dialog.open(CompanyAddModalComponent, {
      width: '500px',
      backdropClass: 'custom-dialog-backdrop-class',
      panelClass: 'custom-dialog-panel-class',
      data: {},
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result.modalData) {
        this.createCompany(result.modalData)
      }
    })
  }
  createCompany(modalData) {
    this.companyService.createCompany(modalData).subscribe((res: any) => {
      console.log(res)
      this.notification.showNotification('Successfully Created', 'success');
      this.fetchCompanies()
    }, (err) => {
      this.notification.showNotification('Error Occured', 'danger');
    } )
  }
}
