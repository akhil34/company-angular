import { Routes } from '@angular/router';
import {CompanyListComponent} from './pages/company-list/company-list.component';
import {CompanySearchComponent} from './pages/company-search/company-search.component';


export const CompanyRoutes: Routes = [
    { path: 'list', component: CompanyListComponent },
    { path: 'search', component: CompanySearchComponent },
]
