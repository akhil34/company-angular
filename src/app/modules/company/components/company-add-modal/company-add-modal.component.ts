import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NotificationService} from '../../../../services/notification/notification.service';

@Component({
  selector: 'app-company-add-modal',
  templateUrl: './company-add-modal.component.html',
  styleUrls: ['./company-add-modal.component.css']
})
export class CompanyAddModalComponent implements OnInit {
  companyForm: FormGroup
  constructor(public dialogRef: MatDialogRef<CompanyAddModalComponent>,
              private formBuilder: FormBuilder,
              private notification: NotificationService) { }
  ngOnInit(): void {
    this.initForm()
  }
  initForm() {
    this.companyForm = this.formBuilder.group({
      title: ['', Validators.required],
      subTitle: ['', Validators.required],
      para: ['', Validators.required],
    })
  }
  submitForm() {
    if (!this.companyForm.valid) {
        this.companyForm.markAllAsTouched();
        this.notification.showNotification('Please fill all fields', 'warning')
      return
    }
    this.dialogRef.close({
      modalData: this.companyForm.value
    })
  }
}
