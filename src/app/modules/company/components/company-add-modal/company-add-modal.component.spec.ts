import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyAddModalComponent } from './company-add-modal.component';

describe('CompanyAddModalComponent', () => {
  let component: CompanyAddModalComponent;
  let fixture: ComponentFixture<CompanyAddModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompanyAddModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyAddModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
