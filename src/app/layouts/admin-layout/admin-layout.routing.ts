import { Routes } from '@angular/router';

import { DashboardComponent } from '../../dashboard/dashboard.component';
export const AdminLayoutRoutes: Routes = [
    { path: 'dashboard',      component: DashboardComponent },
    {
        path: 'user',
        loadChildren: () => import('../../modules/user/user.module').then(m => m.UserModule)
    },
    {
        path: 'company',
        loadChildren: () => import('../../modules/company/company.module').then(m => m.CompanyModule)
    }
];
