import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IndexLayoutRoutes } from './index-layout.routing';
import {LoginComponent} from '../../login/login.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatRippleModule} from '@angular/material/core';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatTooltipModule} from '@angular/material/tooltip';
import {RouterModule} from '@angular/router';
import {IndexLayoutComponent} from './index-layout.component';



@NgModule({
  declarations: [LoginComponent, IndexLayoutComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(IndexLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatRippleModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
  ]
})
export class IndexLayoutModule { }
