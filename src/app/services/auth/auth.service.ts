import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {LocalStorageService} from './local-storage.service';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  api = 'http://localhost:3000/api'
  constructor(private http: HttpClient, private token: LocalStorageService,
              private router: Router) { }

  login(username: string, password: string): Observable<any> {
    return this.http.post<any>(this.api + '/signin', {username, password})
  }
  logout() {
    this.token.removeToken();
    this.router.navigate(['/login']);
  }
}
