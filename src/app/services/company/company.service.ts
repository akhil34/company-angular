import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {
  api = 'http://localhost:3000/api'
  constructor(private http: HttpClient) { }

  getAllCompanies(): Observable<any> {
    return this.http.get<any>(this.api + '/company/list')
  }
  createCompany(company: any): Observable<any> {
    return this.http.post<any>(this.api + '/company/create', company)
  }

}
